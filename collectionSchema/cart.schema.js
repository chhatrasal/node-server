const Mongoose = require('mongoose');

const cartObject = {
    _id: {type: Number},
    uid: {type: Number},
    date: {type: Number, default: Date.now()},
    loginType: {type: String},
    fine: {type: Number}
};

const cartSchema = new Mongoose.Schema(cartObject);

module.exports = Mongoose.model('Cart', cartSchema);