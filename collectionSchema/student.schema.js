const Mongoose = require('mongoose');

const issuedBookObject = {
    bookId: {type: Number},
    isbn: {type: Number},
    issueDate: {
        type: Number, default: Date.now()
    },
    returnDate: {type: Number},
    fine: {type: Number}
};

const IssuedBookSchema = new Mongoose.Schema(issuedBookObject, {_id: false});
module.exports.IssuedBook = Mongoose.model('IssuedObject', IssuedBookSchema);

const studentObject = {
    _id: {type: Number, required: true},
    name: {type: String, required: true, lowercase: true},
    imageUrl : {type: String, default : 'public/profileImage/profile.png'},
    contactNo: {
        type: Number,
        validate: {
            validator: function (contact) {
                return contact >= 6000000000 && contact <= 9999999999;
            }
        }
    },
    email: {type: String, lowercase: true},
    password: {type: String},
    batch: {type: Number},
    branch: {type: String, lowercase: true},
    issueLimit: {type: Number, default: 3},
    issuedBookNos: {
        type: Number,
        default: 0,
        validate: {
            validator: function (length) {
                return length >= 0 && length <= 3;
            }
        }
    },
    issuedBooks: [IssuedBookSchema]
};

const studentSchema = new Mongoose.Schema(studentObject, {_id: false});

module.exports.Student = Mongoose.model('Students', studentSchema, 'Students');