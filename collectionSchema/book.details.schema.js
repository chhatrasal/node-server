const Mongoose = require('mongoose');

const bookDetailsObject = {
    _id: {type: Number},
    bookName: {type: String},
    author: {type: String},
    quantity: {type: Number, default: 0},
    available: {type: Number, default: 0},
    tags: [{type: String}],
    bookIds: [{type: Number}]
};
const bookDetailsSchema = new Mongoose.Schema(bookDetailsObject, {_id: false});
module.exports = Mongoose.model("BookDetails", bookDetailsSchema, "BookDetails");