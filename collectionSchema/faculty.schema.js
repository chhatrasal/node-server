const Mongoose = require('mongoose');

const issuedBookObject = {
    bookId: {type: Number},
    isbn: {type: Number},
    issueDate: {
        type: Number, default: Date.now()
    },
    returnDate: {type: Number},
    fine: {type: Number}
};

const IssuedBookSchema = new Mongoose.Schema(issuedBookObject, {_id: false});

const facultyObject = {
    _id: {type: String},
    name: {type: String},
    imageUrl : {type: String, default : 'public/profileImage/profile.png'},
    contactNo: {
        type: Number,
        validate: {
            validator: function (contact) {
                return contact >= 6000000000 && contact <= 9999999999;
            }
        }
    },
    email: {type: String},
    password: {type: String},
    issueLimit: {type: Number, default: 3},
    issuedBookNos: {
        type: Number,
        default: 0,
        validate: {
            validator: function (length) {
                return length >= 0 && length <= 3;
            }
        }
    },
    branch: {type: String},
    role: {type: String},
    issuedBooks: [IssuedBookSchema]
};

const facultySchema = new Mongoose.Schema(facultyObject, {_id: false});

module.exports = Mongoose.model('Faculty', facultySchema, 'Faculty');