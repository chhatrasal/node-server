const Mongoose = require('mongoose');

const issueIdObject = {
    uid: {type: Number, index: true},
    // name: {type: String},
    issueDate: {
        type: Number, default: Date.now()
    },
    returnDate: {type: Number},
    fine: {type: Number}
};

const issueIdSchema = new Mongoose.Schema(issueIdObject, {_id: false});
module.exports.IssuedSchema = Mongoose.model('IssuedId', issueIdSchema);

const booksObject = {
    _id: {type: Number},
    issued: {type: Boolean, default: false},
    isbn: {type: Number},
    issuedIds: [issueIdSchema]
};

const booksSchema = new Mongoose.Schema(booksObject, {_id: false});
module.exports.Books = Mongoose.model('Books', booksSchema, 'Books');
