const express = require('express');
const bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({extend : true}));
app.use(bodyParser.raw({uploadDir: './public/profileImage'}));

app.use('/app/android/public/profileImage',
    express.static(__dirname + '/public/profileImage'));

app.use('/app/android', require('./router/router.android'));
app.use('/app/desktop', require('./router/router.desktop'));

module.exports = app;
