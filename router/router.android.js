const router = require('express').Router();
const androidScript = require('./script.android');
const Cart = require('../collectionSchema/cart.schema');
const multer = require('multer');

const fileFilter = (request, file, callback) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || file.mimetype === 'image/webp') {
        callback(null, true);
    }else {
        callback(null, false);
    }
};
const storage = multer.diskStorage({
    destination: (request, file, callback) => {
        callback(null, './public/profileImage/');
    },
    filename: (request, file, callback) => {
        callback(null, file.originalname);
    }
});
let upload = multer({storage : storage, fileFilter : fileFilter});

router.post('/login', (request, response, next) => {
    androidScript.login(request.body.userName, request.body.password, request.body.loginType, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result[0]
            });
            // console.log(result);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            next();
        });
});

router.get('/profile', (request, response, next) => {
    androidScript.getProfile(request.query.userName,
        request.query.loginType,
        request.app.get('database'))
        .then(result => {
            if (result[0] === null || result[0] === undefined) {
                response.json({
                    code: 500,
                    message: 'Error',
                    data: null
                });
            } else {
                response.json({
                    code: 200,
                    message: 'Success',
                    data: result[0]
                });
            }
            // console.log(result[0]);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            next();
        });
});
router.post('/uploadImage', upload.single('image'), (request, response, next) => {
    let url = request.file.path;
    console.log(url);
    console.log(url.replace(/\\/g,"/"));
    androidScript.uploadImage(request.body.id,
        request.file.path,
        request.body.loginType, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: url.replace(/\\/g,"/")
            });
        }).catch(error => {
        response.json({
            code: 500,
            message: 'Error',
            data: error
        });
        next();
    });

});
router.post('/profile', (request, response, next) => {
    androidScript.updateProfile(request.body, request.body.loginType, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            next();
        });
});

router.get('/bookList', (request, response, next) => {
    androidScript.getBookList(request.app.get('database'))
        .then(result => {
            // console.log(`in router \n ${result}`);
            response.json({
                code: 200,
                message: 'Inserted.',
                data: result
            });
        })
        .catch(error => {
            // console.log(`in router \t ${error}`);
            response.json({
                code: 500,
                message: 'Server internal error.',
                data: error
            });
            next();
        });
});

router.post('/issueBook', (request, response, next) => {
    let cartIds = [];
    let cartItemList = [];
    for (let index = 0; index < request.body.length; ++index) {
        cartIds.push(request.body[index]._id);
        cartItemList.push(new Cart(request.body[index]));
    }
    androidScript.issueBook(cartItemList, cartIds, request.app.get('database'))
        .then(result => {
            // console.log(`in router`);
            // console.log(result);
            if (result !== null || result !== undefined) {
                // console.log("in the if condition");
                response.json({
                    code: 200,
                    message: 'Inserted.',
                    data: result
                });
            } else {
                // console.log("in the else condition");
                response.json({
                    code: 200,
                    message: 'The book is already issued.',
                    data: null
                });
            }
        })
        .catch(error => {
            // console.log(`in router error`);
            // console.log(error);
            if (error === null || error === undefined) {
                response.json({
                    code: 500,
                    message: 'The book is already Issued.',
                    data: null
                });
            } else {
                response.json({
                    code: 500,
                    message: 'Server internal error.',
                    data: error
                });
            }
            next();
        });
});

router.post('/returnBook', (request, response, next) => {
    let cartList = [];
    for (let index = 0; index < request.body.length; ++index) {
        request.body[index].fine = 10;
        cartList.push(new Cart(request.body[index]));
    }
    androidScript.returnBook(cartList, request.app.get('database'))
        .then(result => {
            // console.log(result);
            response.json({
                code: 200,
                message: 'Inserted.',
                data: result
            });
        })
        .catch(error => {
            // console.log(error);
            response.json({
                code: 500,
                message: 'Server internal error.',
                data: error
            });
            next();
        });
});

router.get('/issuedBookList', (request, response, next) => {
    // console.log(request.query);
    androidScript.getIssuedBookList(request.query.uid, request.query.loginType, request.app.get('database'))
        .then(result => {
            // console.log('success');
            // console.log(result);
            if (result) {
                response.json({
                    code: 200,
                    message: "Success",
                    data: result
                });
            } else {
                response.json({
                    code: 200,
                    message: "Success no element found.",
                    data: null
                });
            }
        })
        .catch(error => {
            // console.log(error);
            if (error) {
                response.json({
                    code: 500,
                    message: "Internal Server Error.",
                    data: error
                });
            } else {
                response.json({
                    code: 500,
                    message: "Internal Server Error.",
                    data: null
                });
            }
            next();
        });
});

router.get('/issueCartList', (request, response, next) => {
    androidScript.issueCartList(request.query.uid, request.query.loginType, request.app.get('database'))
        .then(result => {
            if (result) {
                response.json({
                    code: 200,
                    message: "Success",
                    data: result
                });
            }
        })
        .catch(error => {
            if (error) {
                response.json({
                    code: 500,
                    message: "Internal Server Error.",
                    data: error
                });
            }
            next();
        });
});

router.get('/returnCartList', (request, response, next) => {
    androidScript.returnCartList(request.query.uid, request.query.loginType, request.app.get('database'))
        .then(result => {
            if (result) {
                response.json({
                    code: 200,
                    message: "Success",
                    data: result
                });
            }
        })
        .catch(error => {
            if (error) {
                response.json({
                    code: 500,
                    message: "Internal Server Error.",
                    data: error
                });
            }
            next();
        });
});
//dev apis
router.get('/deleteCollection/:name', (request, response, next) => {
    androidScript.deleteCollection(request.params.name, request.app.get('database'))
        .then(result => {
            // console.log(`in router \t ${result}`);
            response.json({
                code: 200,
                message: 'Inserted.',
                data: result
            });
        })
        .catch(error => {
            // console.log(`in router \t ${error}`);
            response.json({
                code: 500,
                message: 'Server internal error.',
                data: error
            });
            next();
        });
});

router.get('/listCollection/:name', (request, response, next) => {
    androidScript.listCollection(request.params.name, request.app.get('database'))
        .then(result => {
            // console.log(`in router \t ${result}`);
            response.json({
                code: 200,
                message: 'Data Fetched.',
                data: result
            });
        })
        .catch(error => {
            // console.log(`in router \t ${error}`);
            response.json({
                code: 500,
                message: 'Server internal error.',
                data: error
            });
            next();
        });
});

router.post('/removeCartBook', (request, response) => {
    // console.log(request.body);
    androidScript.removeCartBook(request.body.uid, request.body.bookId,
        request.body.collectionName, request.app.get('database'))
        .then(result => {
            if (result) {
                response.json({
                    code: 200,
                    message: "Success",
                    data: result
                });
            } else {
                response.json({
                    code: 200,
                    message: "Success",
                    data: "No content found"
                });
            }
        })
        .catch(error => {
            if (error) {
                response.json({
                    code: 500,
                    message: "Internal Server Error.",
                    data: error
                });
            }
        });
});

// router.post('/upload', (request, response, next) => {
//     console.log(request.body);
//     // console.log(request.files.image.path);
//     response.send("Api completed.");
// });
module.exports = router;