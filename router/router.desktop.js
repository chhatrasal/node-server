const router = require('express').Router();
const desktopScript = require('./script.desktop');
const Student = require('../collectionSchema/student.schema').Student;
const StudentIssuedSchema = require('../collectionSchema/student.schema').IssuedBook;
const Faculty = require('../collectionSchema/faculty.schema');
const Book = require('../collectionSchema/books.schema').Books;
const BookData = require('../collectionSchema/book.details.schema');
const Cart = require('../collectionSchema/cart.schema');
const IssuedSchema = require('../collectionSchema/books.schema').IssuedSchema;

// add books to the database.
router.post('/book', (request, response, next) => {
    request.body.available = request.body.quantity;
    if (request.body.bookId === null || request.body.bookId === undefined) {
        request.body.bookIds = [];
        request.body.books = [];
        for (let index = request.body.start; index <= request.body.end; ++index) {
            request.body.bookIds.push(index);
            request.body.books.push(new Book({
                _id: index,
                isbn: request.body._id
            }));
        }
        if (request.body.quantity !== request.body.bookIds.length) {
            response.json({
                code: 500,
                message: "Internal Server Error",
                data: "The quantity and book ids are not same. Please check the quantity and book id's ranges."
            });
        } else {
            desktopScript.insertBook(new BookData(request.body), request.body.books, request.app.get('database'))
                .then(result => {
                    response.json({
                        code: 200,
                        message: 'Inserted.',
                        data: result
                    });
                })
                .catch(error => {
                    // console.log(error);
                    response.json({
                        code: 500,
                        message: 'Server internal error.',
                        data: error
                    });
                    next();
                });
        }
    } else {
        let book = [];
        book.push(new Book({_id: request.body.bookId, isbn: request.body._id}));
        request.body.bookIds = [];
        request.body.bookIds.push(request.body.bookId);

        desktopScript.insertBook(new BookData(request.body), book, request.app.get('database'))
            .then(result => {
                response.json({
                    code: 200,
                    message: 'Inserted.',
                    data: result
                });
            })
            .catch(error => {
                // console.log(error.message);
                response.json({
                    code: 500,
                    message: 'Server internal error.',
                    data: error.message
                });
                next();
            });
    }
});

// get book details from isbn
router.get('/book', (request, response, next) => {
    desktopScript.getBookDetails(request.query.isbn, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error.message
            });
            next();
        });

});

// router.post('/book', (request, response, next) => {
//     request.body.bookIds = [];
//     request.body.book = [];
//     for (let index = request.body.start; index <= request.body.end; ++index) {
//         request.body.bookIds.push(index);
//         request.body.book.push(new Book({
//             _id: index,
//             isbn: request.body.isbn
//         }));
//     }
//     if (request.body.quantity !== request.body.bookIds) {
//         response.json({
//             code: 500,
//             message: "Internal Server Error",
//             data: "The quantity and book ids are not same. Please check the quantity and book id's ranges."
//         });
//     } else {
//         desktopScript.updateBookDetails(request.body, request.app.get('database'))
//             .then(result => {
//                 response.json({
//                     code: 200,
//                     message: 'Success',
//                     data: result
//                 });
//                 console.log(result);
//             })
//             .catch((error) => {
//                 response.json({
//                     code: 500,
//                     message: 'Error',
//                     data: error
//                 });
//                 next();
//             });
//     }
// });

router.post('/profile', (request, response, next) => {
    request.body.password = request.body._id;
    if (request.body.loginType === 'faculty') {
        desktopScript.insertUser(new Faculty(request.body), request.body.loginType, request.app.get('database'))
            .then(result => {
                response.json({
                    code: 200,
                    message: 'Inserted.',
                    data: result
                });
            })
            .catch(error => {
                response.json({
                    code: 500,
                    message: 'Server internal error.',
                    data: error.message
                });
                next();
            });
    } else if (request.body.loginType === 'student') {
        desktopScript.insertUser(new Student(request.body), request.body.loginType, request.app.get('database'))
            .then(result => {
                response.json({
                    code: 200,
                    message: 'Inserted.',
                    data: result
                });
            })
            .catch(error => {
                response.json({
                    code: 500,
                    message: 'Server internal error.',
                    data: error.message
                });
                next();
            });
    }
});

router.post('/updateProfile', (request, response, next) => {
    // console.log(request.body);
    desktopScript.updateProfileLibrarian(request.body.oldId, request.body, request.body.loginType, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error.message
            });
            next();
        });
});

router.post('/updateBook', (request, response, next) => {
    // console.log(request.body);
    desktopScript.updateBookLibrarian(request.body.oldId, request.body, request.body.loginType, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error.message
            });
            next();
        });
});

router.post('/removeProfile', (request, response, next) => {
    // console.log(request.body);
    desktopScript.removeProfile(request.body.uid, request.body.loginType,
        request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch(error => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            next();
        });
});
router.get('/cartList', (request, response, next) => {
    if (request.query.purpose === 'issueCart') {
        desktopScript.issueCartList(request.query.id, request.query.loginType, request.app.get('database'))
            .then(result => {
                if (result === null || result === undefined) {
                    response.json({
                        code: 500,
                        message: 'Internal Server Error',
                        data: null
                    });
                } else {
                    response.json({
                        code: 200,
                        message: 'Success',
                        data: result
                    });
                }
                // console.log(result);
            })
            .catch((error) => {
                response.json({
                    code: 500,
                    message: 'Error',
                    data: error
                });
                // console.log(error);
                next();
            });
    } else if (request.query.purpose === 'returnCart') {
        desktopScript.returnCartList(request.query.id, request.query.loginType, request.app.get('database'))
            .then(result => {
                if (result) {
                    response.json({
                        code: 200,
                        message: 'Success',
                        data: result
                    });
                } else {
                    response.json({
                        code: 500,
                        message: 'Internal Server Error',
                        data: null
                    });
                }
                // console.log(result);
            })
            .catch((error) => {
                response.json({
                    code: 500,
                    message: 'Error',
                    data: error
                });
                // console.log(error);
                next();
            });
    }
});

router.post('/confirmIssueBooks', (request, response, next) => {
    let studentIssued = [];
    let issuedBook = [];
    for (let index = 0; index < request.body.length; ++index) {
        studentIssued.push(new StudentIssuedSchema({
            bookId: request.body[index].bookId,
            isbn: request.body[index].isbn
        }));
        issuedBook.push(new IssuedSchema({uid: request.body[index].uid}));
    }
    desktopScript.confirmIssueBook(request.body, studentIssued, issuedBook, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch(error => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            // console.log(error);
            next();
        });
});

router.get('/profile', (request, response, next) => {
    desktopScript.getProfile(request.query.userName, request.query.loginType, request.app.get('database'))
        .then(result => {
            if (result[0] === null || result[0] === undefined) {
                response.json({
                    code: 500,
                    message: 'Error',
                    data: null
                });
            } else {
                response.json({
                    code: 200,
                    message: 'Success',
                    data: result[0]
                });
            }
            // console.log(result[0]);
        })
        .catch((error) => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            next();
        });
});

router.post('/confirmReturnBooks', (request, response, next) => {
    let returnBookList = [];
    for (let index = 0; index < request.body.length; ++index) {
        request.body[index].fine = calculateFine(request.body[index].issuedDate);
        returnBookList.push(new Cart(request.body[index]));
    }
    desktopScript.confirmReturnBook(returnBookList, request.app.get('database'))
        .then(result => {
            response.json({
                code: 200,
                message: 'Success',
                data: result
            });
            // console.log(result);
        })
        .catch(error => {
            response.json({
                code: 500,
                message: 'Error',
                data: error
            });
            // console.log(error);
            next();
        });
});


module.exports = router;

let calculateFine = (issueDate) => {
    let returnDate = parseInt("" + Date.now() / (1000 * 60 * 60 * 24));
    let issuedDate = parseInt("" + (new Date(issueDate).getTime() / (1000 * 60 * 60 * 24)));
    let totalDays = returnDate - issuedDate;
    let fine = 0;
    if (totalDays > 90) {
        fine = (totalDays - 90) * 5;
    }
    // console.log("fine = " + fine);
    return fine;
};