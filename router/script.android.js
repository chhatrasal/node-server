module.exports = {
    login: (username, password, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .find({
                    _id: parseInt(username),
                    password: password
                })
                .toArray()
                .then(result => {
                    if (result) {
                        console.log(result);
                        resolve(result);
                    } else {
                        throw new Error("Please enter valid credentials.");
                    }
                })
                .catch(error => {
                    reject(error);
                });
        })
    },

    getBookList: (database) => {
        return new Promise((resolve, reject) => {
            database.collection('bookDetails')
                .find({}, {issuedIds: 0})
                .toArray()
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    getIssuedBookList: (id, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .aggregate({
                    $match: {_id: parseInt(id)}
                }, {
                    $lookup: {
                        from: 'bookDetails',
                        // localField: 'issuedBooks.isbn',
                        // foreignField: '_id',
                        // let: {isbn: '$issuedBooks.isbn'},
                        pipeline: [
                            // {$match: {'_id' : '$$isbn'}},
                            {$project: {author: true, bookName: true}}
                        ],
                        as: 'bookInfo'
                    }
                }, {
                    $project: {'bookInfo.author': true}
                })
                .toArray()
                .then(result => {
                    // console.log(result);
                    if (result) {
                        let outputList = [];
                        for (let index1 = 0; index1 < result[0].issuedBooks.length; ++index1) {
                            for (let index2 = 0; index2 < result[0].bookInfo.length; ++index2) {
                                if (result[0].issuedBooks[index1].isbn === result[0].bookInfo[index2]._id) {
                                    outputList.push({
                                        issueDate: result[0].issuedBooks[index1].issueDate,
                                        bookId: result[0].issuedBooks[index1].bookId,
                                        returnDate: result[0].issuedBooks[index1].returnDate,
                                        isbn: result[0].issuedBooks[index1].isbn,
                                        bookName: result[0].bookInfo[index2].bookName,
                                        author: result[0].bookInfo[index2].author,
                                        fine: result[0].issuedBooks[index1].fine
                                    });
                                }
                            }
                        }
                        resolve(outputList);
                    } else {
                        throw new Error();
                    }
                })
                .catch(error => {
                    // console.log(error);
                    reject(error)
                })
        });
    },

    getProfile: (userName, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .find({
                    _id: parseInt(userName)
                })
                .toArray()
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },

    updateProfile: (updateInfo, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .update({
                    _id: updateInfo._id,
                    password: updateInfo.oldPassword
                }, {
                    $set: {
                        email: updateInfo.email,
                        contactNo: parseInt(updateInfo.contactNo),
                        password: updateInfo.newPassword
                    }
                }, {
                    $upsert: false
                })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    issueBook: (cart, cartIds, database) => {
        let updateItemNos;
        let updateIds = {ids: [], isbn: []};
        return new Promise((resolve, reject) => {
            database.collection('book').find({_id: {$in: cartIds}, issued: false})
                .toArray()
                .then((result) => {
                    // console.log("the read query is successful.");
                    // console.log(result);
                    updateItemNos = result.length;
                    for (let index = 0; index < result.length; ++index) {
                        updateIds.ids.push(result[index]._id);
                        updateIds.isbn.push(result[index].isbn);
                    }
                    // console.log(updateIds);
                    if (result) {
                        return database.collection('issueCart')
                            .insertMany(cart);
                    } else {
                        throw new Error("The book is already available");
                    }
                })
                .then(result => {
                    // console.log("cart item inserted successfully.");
                    // console.log(result);
                    if (result) {
                        if (result.insertedCount) {
                            if (result.insertedCount > 0) {
                                // console.log('inserted count' + result.insertedCount);
                                return database.collection('book')
                                    .updateMany({_id: {$in: updateIds.ids}}, {$set: {issued: true}}, {$upsert: false});
                            }
                        }
                    }
                })
                .then(result => {
                    // console.log("Everything is successful");
                    // console.log(result);
                    if (result) {
                        if (result.result) {
                            if (result.result.nModified) {
                                if (result.result.nModified >= 1) {
                                    resolve(result);
                                } else {
                                    return database.collection('issueCart').deleteMany({_id: {$in: cartIds}});
                                }
                            } else {
                                return database.collection('issueCart').deleteMany({_id: {$in: cartIds}});
                            }
                        }
                    }
                })
                .then(result => {
                    if (result) {
                        if (result.deletedCount) {
                            if (result.deletedCount > 0) {
                                return database.collection('book').updateMany({_id: {$in: cartIds}}, {$set: {issued: false}}, {$upsert: false});
                            }
                        }
                    }
                })
                .then(result => {
                    if (result) {
                        if (result.result) {
                            if (result.result.nModified > 0) {
                                resolve(result);
                            }
                        }
                    } else {
                        throw new Error();
                    }
                })
                .catch(error => {
                    // console.log("An error occurred.");
                    reject(error);
                });
        });
    },

    returnBook: (cart, database) => {
        let returnIds = [];
        for (let index = 0; index < cart.length; ++index) {
            returnIds.push(cart[index]._id);
        }

        return new Promise((resolve, reject) => {
            database.collection('book')
                .find({_id: {$in: returnIds}, issued: true})
                .toArray()
                .then(result => {
                    // console.log("the read query is successful.");
                    if (result) {
                        // console.log("the book is available.");
                        return database.collection('returnCart')
                            .insert(cart);
                    } else {
                        // console.log("the book is not available.");
                        throw new Error();
                    }
                })
                .then(result => {
                    // console.log("Everything is successful");
                    resolve(result);
                })
                .catch(error => {
                    // console.log("An error occurred.");
                    reject(error);
                });
        });
    },

    issueCartList: (id, userCollection, database) => {
        return new Promise((resolve, reject) => {
            database.collection('issueCart')
                .aggregate([
                    {
                        $match: {uid: parseInt(id)}
                    }, {
                        $project: {date: 0}
                    }, {
                        $lookup: {
                            from: "bookDetails",
                            localField: "_id",
                            foreignField: "bookIds",
                            as: "book"
                        },
                    }, {
                        $unwind: "$book"
                    }, {
                        $replaceRoot: {
                            newRoot: {
                                $mergeObjects: ["$book", "$$ROOT"]
                            }
                        }
                    }, {
                        $project: {"bookId": "$_id", author: "$author", name: "$bookName", _id: 0}
                    }
                ])
                .toArray()
                .then(result => {
                    // console.log(output);
                    resolve(result);
                })
                .catch(error => {
                    // console.log(error);
                    reject(error);
                });
        });
    },

    returnCartList: (id, userCollection, database) => {
        return new Promise((resolve, reject) => {
            database.collection('returnCart')
                .aggregate([
                    {
                        $match: {uid: parseInt(id)}
                    }, {
                        $project: {date: 0}
                    }, {
                        $lookup: {
                            from: "bookDetails",
                            localField: "_id",
                            foreignField: "bookIds",
                            as: "book"
                        },
                    }, {
                        $unwind: "$book"
                    }, {
                        $replaceRoot: {
                            newRoot: {
                                $mergeObjects: ["$book", "$$ROOT"]
                            }
                        }
                    }, {
                        $project: {"bookId": "$_id", author: "$author", name: "$bookName", _id: 0}
                    }
                ])
                .toArray()
                .then(result => {
                    // console.log(output);
                    resolve(result);
                })
                .catch(error => {
                    // console.log(error);
                    reject(error);
                });
        });
    },

    uploadImage: (id, url, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .update({_id: parseInt(id)},
                    {$set: {imageUrl: url}},
                    {$upsert: false})
                .then(result => {
                    if (result)
                        resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    removeCartBook: (uid, bookId, collectionName, database) => {
        return new Promise((resolve, reject) => {
            // console.log('inside the log')
            database.collection(collectionName)
                .findOneAndDelete({_id: parseInt(bookId), uid: parseInt(uid)})
                .then(result => {
                    console.log(result);
                    if (result) {
                        if (result.lastErrorObject) {
                            if (result.lastErrorObject.n) {
                                if (result.lastErrorObject.n === 1) {
                                    return database.collection('book')
                                        .update({_id: result.value._id},
                                            {$set: {issued: false}},
                                            {$upsert: false})
                                }
                            }
                        }
                        resolve(result);
                    }
                })
                .then(result => {
                    if (result) {
                        resolve(result);
                    }
                })
                .catch(error => {
                    // console.log(error);
                    reject(error);
                });
        });
    },
    //dev apis
    deleteCollection: (name, database) => {
        return new Promise((resolve, reject) => {
            database.collection(name)
                .drop()
                .then(result => {
                    // console.log(result);
                    resolve(result)
                })
                .catch(error => {
                    // console.log(error);
                    reject(error)
                });
        });

    },

    listCollection: (name, database) => {
        return new Promise((resolve, reject) => {
            database.collection(name)
                .find()
                .toArray()
                .then(result => {
                    // console.log(result);
                    resolve(result)
                })
                .catch(error => {
                    // console.log(error);
                    reject(error)
                });
        });
    }

};