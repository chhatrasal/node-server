module.exports = {

    insertBook: (isbnBook, book, database) => {
        return new Promise(((resolve, reject) => {
            database.collection('bookDetails')
                .findOne({_id: isbnBook._id})
                .then(result => {
                    if (result) {
                        return database.collection('bookDetails')
                            .update({_id: isbnBook._id}, {$addToSet: {bookIds: {$each: isbnBook.bookIds}}}, {$upsert: false});
                    } else {
                        return database.collection('bookDetails').insert(isbnBook);
                    }
                })
                .then(result => {
                    if (result) {
                        if (result.result) {
                            if (result.result.nModified) {
                                if (result.result.nModified === 1) {
                                    return database.collection('book').insert(book);
                                }
                            }
                        }
                        if (result.insertedCount) {
                            if (result.insertedCount > 0) {
                                return database.collection('book').insert(book);
                            }
                        }
                    }
                })
                .then(result => {
                    if (result) {
                        resolve(result);
                    } else {
                        throw new Error("The book already exists in the database.");
                    }
                })
                .catch(error => {
                    reject(error);
                });
        }));
    },

    getBookDetails: (id, database) => {
        return new Promise((resolve, reject) => {
            database.collection('bookDetails')
                .findOne({_id: parseInt(id)})
                .then(result => {
                    if (result) {
                        resolve(result)
                    } else {
                        throw new Error("Please enter correct isbn number.");
                    }
                })
                .catch(error => reject(error))
        })
    },

    issueCartList: (id, userCollection, database) => {
        return new Promise((resolve, reject) => {
            database.collection('issueCart')
                .aggregate([{
                    $match: {uid: parseInt(id)}
                }, {
                    $lookup: {
                        from: "bookDetails",
                        localField: "_id",
                        foreignField: "bookIds",
                        as: "book"
                    },
                }, {
                    $unwind: "$book"
                }, {
                    $replaceRoot: {
                        newRoot: {
                            $mergeObjects: ["$book", "$$ROOT"]
                        }
                    }
                }, {
                    $lookup: {
                        from: userCollection,
                        localField: "uid",
                        foreignField: "_id",
                        as: "student"
                    }
                }, {
                    $unwind: "$student"
                }, {
                    $replaceRoot: {
                        newRoot: {
                            $mergeObjects: ["$student", "$$ROOT"]
                        }
                    }
                }, {
                    $project: {
                        bookName: 1, author: 1, name: 1, batch: 1, branch: 1,
                        uid: 1, loginType: 1, date: 1
                    }
                }, {
                    $sort: {addedDate: -1}
                }, {
                    $limit: 3
                }])
                .toArray()
                .then(result => {
                    if (result) {
                        console.log(result);
                        // if (result.length !== 0) {
                        //     let output = {
                        //         uid: Number,
                        //         name: String,
                        //         batch: Number,
                        //         branch: String,
                        //         issuedBooks: []
                        //     };
                        //     for (let index = 0; index < result.length; ++index) {
                        //         output.uid = result[index].uid;
                        //         output.name = result[index].name;
                        //         output.batch = result[index].batch;
                        //         output.branch = result[index].branch;
                        //         output.loginType = result[index].loginType;
                        //         output.issuedBooks.push({
                        //             bookId: result[index]._id,
                        //             author: result[index].author,
                        //             isbn: result[index].isbn,
                        //             name: result[index].bookName
                        //         });
                        //     }
                        //     resolve(result);
                        // } else {
                        //     return database.collection(userCollection)
                        //         .find({_id: parseInt(id)})
                        //         .project({uid: '_id', batch: true, branch: true, name: true})
                        //         .toArray()
                        // }
                        resolve(result);
                    }
                })
                .then(result => {
                    if (result) {
                        // result[0].issuedBooks = [];
                        resolve(result[0]);
                    }
                })
                .catch(error => {
                    reject(error);
                });
        });
    },

    returnCartList: (id, userCollection, database) => {
        return new Promise(((resolve, reject) => {
            database.collection('returnCart')
                .aggregate({
                    $match: {uid: parseInt(id)}
                }, {
                    $lookup: {
                        from: userCollection,
                        localField: "uid",
                        foreignField: "_id",
                        // let: {uid: "$uid"},
                        // pipeline: [
                        //     {$match: {$expr: {$eq: ["$_id", "$$uid"]}}},
                        //     {$project : { "issuedBooks.issueDate" : 1}}
                        // ],
                        as: 'studentData'
                    }
                }, {
                    $project: {studentData: 1, date: 1, uid: 1, loginType: 1}
                }, {
                    $lookup: {
                        from: "bookDetails",
                        let: {bookId: "$_id"},
                        pipeline: [
                            // {$match: {$expr: {$eq: ["$isbn", "$$bookId"]}}},
                            // {$match: {$expr: {$eq: ["$bookIds", "$$bookId"]}}},
                            {$project: {_id: 1, author: 1, bookName: 1}}],
                        as: "book"
                    }
                }, {
                    $project: {book: 1, studentData: 1, date: 1, uid: 1, loginType: 1}
                    // }, {
                    //     $project: {date: 1, _id : 1, loginType : 1}
                }, {
                    $match: {uid: parseInt(id)}
                })
                .toArray()
                .then(result => {
                    // console.log(result);
                    if (result) {
                        // console.log(new Date(result[0].date));
                        if (result.length !== 0) {
                            let output = {
                                uid: Number,
                                name: String,
                                batch: Number,
                                branch: String,
                                issuedBooks: []
                            };
                            for (let index1 = 0; index1 < result.length; ++index1) {
                                output.uid = result[index1].uid;
                                output.name = result[index1].studentData[0].name;
                                output.batch = result[index1].studentData[0].batch;
                                output.branch = result[index1].studentData[0].branch;
                                for (let index2 = 0; index2 < result[index1].studentData[0].issuedBooks.length; ++index2) {
                                    if (result[index1]._id === result[index1].studentData[0].issuedBooks[index2].bookId) {
                                        // console.log('hiii');
                                        output.issuedBooks.push({
                                            bookId: result[index1]._id,
                                            bookName: result[index1].book[0].bookName,
                                            author: result[index1].book[0].author,
                                            isbn: result[index1].studentData[0].issuedBooks[index2].isbn,
                                            issueDate: result[index1].studentData[0].issuedBooks[index2].issueDate,
                                        });
                                    }
                                }
                            }
                            resolve(output);
                        } else {
                            return database.collection(userCollection)
                                .find({_id: parseInt(id)})
                                .project({uid: '_id', batch: true, branch: true, name: true})
                                .toArray()
                        }
                    } else {
                        throw new Error();
                    }
                })
                .then(result => {
                    if (result) {
                        result[0].issuedBooks = [];
                        resolve(result[0]);
                    }
                })
                .catch(error => reject(error));
        }));
    },

    insertUser: (user, collectionName, database) => {
        return new Promise(((resolve, reject) => {
            database.collection(collectionName)
                .insert(user)
                .then(result => {
                    if (result) {
                        resolve(result)
                    } else {
                        throw new Error("Please enter the valid details.");
                    }
                })
                .catch(error => reject(error));
        }))
    },

    // insertStudents: (student, database) => {
    //     return new Promise((resolve, reject) => {
    //         database.collection('student')
    //             .insert(student)
    //             .then(result => {
    //                 resolve(result)
    //             })
    //             .catch(error => {
    //                 reject(error)
    //             });
    //     });
    // },

    // insertTeachers: (teacher, database) => {
    //     return new Promise((resolve, reject) => {
    //         database.collection('teacher')
    //             .insert(teacher)
    //             .then(result => {
    //                 resolve(result)
    //             })
    //             .catch(error => {
    //                 reject(error)
    //             });
    //     });
    // },

    updateProfileLibrarian: (oldId, updateInfo, collectionName, database) => {
        // console.log(updateInfo);
        if (!oldId) {
            oldId = updateInfo._id;
        }
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .findOne({_id: oldId})
                .then(result => {
                    if (result) {
                        // console.log(result);
                        if (updateInfo.name) {
                            result.name = updateInfo.name;
                        }
                        if (updateInfo.batch) {
                            result.batch = updateInfo.batch;
                        }
                        if (updateInfo.branch) {
                            result.branch = updateInfo.branch;
                        }
                        if (updateInfo.contactNo) {
                            result.contactNo = updateInfo.contactNo;
                        }
                        if (updateInfo.password) {
                            result.password = updateInfo.password;
                        }
                        if (result._id === updateInfo._id) {
                            return database.collection(collectionName)
                                .update({_id: updateInfo._id}, result)
                        } else if (result._id !== updateInfo._id) {
                            result._id = updateInfo._id;
                            return database.collection(collectionName).insertOne(result);
                        }
                    } else {
                        throw new Error("No data found");
                    }
                })
                .then(result => {
                    // console.log("in the desktop api");
                    // console.log(result);
                    if (result) {
                        if (result.result) {
                            if (result.result.nModified) {
                                if (result.result.nModified > 0) {
                                    resolve("The profile is updated successfully.")
                                }
                            } else if (result.result.n) {
                                if (result.result.n) {
                                    if (result.result.n > 0) {
                                        return database.collection(collectionName)
                                            .findOneAndDelete({_id: oldId});
                                    }
                                }

                            }
                        }
                    }
                })
                .then(result => {
                    // console.log(result);
                    if (result) {
                        resolve("The profile is updated successfully.")
                    } else {
                        throw new Error("No data found")
                    }
                })
                .catch(error => {
                    // console.log(error);
                    reject(error);
                });
        });
    },

    updateBookLibrarian: (oldId, updateInfo, collectionName, database) => {
        // console.log(updateInfo);
        if (!oldId) {
            oldId = updateInfo._id;
        }
        let oldBooks = [];
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .findOne({_id: oldId})
                .then(result => {
                    if (result) {
                        // console.log(result);
                        if (updateInfo.bookName) {
                            result.bookName = updateInfo.bookName;
                        }
                        if (updateInfo.tags) {
                            result.tags = updateInfo.tags;
                        }
                        if (updateInfo.author) {
                            result.author = updateInfo.author;
                        }
                        if (updateInfo.quantity) {
                            result.available = result.available + result.quantity - updateInfo.quantity;
                            result.quantity = updateInfo.quantity;
                        }
                        if (updateInfo.start && updateInfo.end) {
                            oldBooks = result.bookIds;
                            result.bookIds = updateInfo.bookIds;
                        } else if (updateInfo.oldBookId) {
                            if (result.bookIds.indexOf(updateInfo.oldBookId) > -1) {
                                result.bookIds.splice(result.bookIds.indexOf(updateInfo.oldBookId), 1);
                                result.bookIds.push(updateInfo.newBookId);
                            }
                        }

                        if (updateInfo.password) {
                            result.password = updateInfo.password;
                        }
                        if (result._id === updateInfo._id) {
                            return database.collection(collectionName)
                                .update({_id: updateInfo._id}, result)
                        } else if (result._id !== updateInfo._id) {
                            result._id = updateInfo._id;
                            return database.collection(collectionName).insertOne(result);
                        }
                    } else {
                        throw new Error("No data found");
                    }
                })
                .then(result => {
                    // console.log("in the desktop api");
                    // console.log(result);
                    if (result) {
                        if (result.result) {
                            if (result.result.nModified) {
                                if (result.result.nModified > 0) {
                                    resolve("The profile is updated successfully.")
                                }
                            } else if (result.result.n) {
                                if (result.result.n) {
                                    if (result.result.n > 0) {
                                        return database.collection(collectionName)
                                            .findOneAndDelete({_id: oldId});
                                    }
                                }

                            }
                        }
                    }
                })
                .then(result => {
                    // console.log(result);
                    if (result) {
                        resolve("The profile is updated successfully.")
                    } else {
                        throw new Error("No data found")
                    }
                })
                .catch(error => {
                    // console.log(error);
                    reject(error);
                });
        });
    },

    removeProfile: (id, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .findOneAndDelete({_id: parseInt(id)})
                .then(result => {
                    // console.log("1");
                    if (result) {
                        // console.log("2");
                        // console.log(result);
                        if (result.lastErrorObject) {
                            // console.log("3");
                            // console.log(result.lastErrorObject.n);
                            if (result.lastErrorObject.n > 0) {
                                // console.log("5");
                                resolve("The profile is deleted successfully.");
                            } else {
                                // console.log('indeide error');
                                throw new Error("No record found for the given uid.");
                            }

                        }
                    }

                }).catch(error => {
                reject(error.message);
            });
        });
    },
    updateBookDetails: (updateDetails, database) => {
        return new Promise((resolve, reject) => {
            database.collection('bookDetails').update({_id: updateDetails.isbn},
                {
                    $set: {
                        bookName: updateDetails.bookName,
                        author: updateDetails.author
                    },
                    $addToSet: {
                        tags: updateDetails.tags,
                        bookIds: updateDetails.bookIds
                    }
                },
                {$upsert: false})
                .then((result) => {
                    if (result) {
                        if (result.result) {
                            if (result.nModified) {
                                if (result.result.nModified > 0) {
                                    return database.collection('book')
                                        .insert(updateDetails.book)
                                }
                            }
                        }
                    }
                })
                .then(result => {
                    if (result) {

                    }
                })
                .then(result => resolve(result))
                .catch(error => reject(error));
        })
    },

    getProfile: (userName, collectionName, database) => {
        return new Promise((resolve, reject) => {
            database.collection(collectionName)
                .find({
                    _id: parseInt(userName)
                })
                .toArray()
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                });
        })
    },

    confirmIssueBook: (updateInfo, userData, bookData, database) => {
        let removeBookIds = [];
        for (let index = 0; index < updateInfo.length; ++index) {
            removeBookIds.push(updateInfo[index].bookId);
        }
        return new Promise((resolve, reject) => {
            // console.log("In the promise");
            database.collection('issueCart')
                .remove({
                    _id: {$in: removeBookIds}
                })
                .then(result => {
                    // console.log("element found");
                    // console.log(result);
                    if (result) {
                        if (result.result) {
                            // console.log("updating student collection");
                            return database.collection(updateInfo[0].loginType)
                                .update({
                                    _id: updateInfo[0].uid
                                }, {
                                    $inc: {issuedBookNos: userData.length},
                                    $addToSet: {
                                        issuedBooks: {$each: userData}
                                    }
                                });
                        } else {
                            throw new Error();
                        }
                    } else {
                        throw new Error();
                    }
                })
                .then(result => {
                    if (result) {
                        // console.log("Student collection updated.");
                        // console.log(result);
                        if (result.result) {
                            if (result.result.nModified) {
                                if (result.result.nModified > 0) {
                                    // console.log("updating book collection");
                                    for (let index = 0; index < userData.length; ++index) {
                                        database.collection('book')
                                            .update({
                                                _id: removeBookIds[index]
                                            }, {
                                                $addToSet: {
                                                    issuedIds: bookData[index]
                                                }
                                            }, {
                                                $upsert: false
                                            });
                                        database.collection('bookDetails').update({
                                            _id: userData[index].isbn
                                        }, {
                                            $inc: {available: -1}
                                        }, {
                                            $upsert: false
                                        })
                                    }
                                    return true;
                                } else {
                                    throw new Error();
                                }
                            } else {
                                throw new Error();
                            }
                        } else {
                            throw new Error();
                        }
                    } else {
                        throw new Error();
                    }
                })
                .then(result => {
                    // console.log(result);
                    if (result) {
                        resolve("");
                    } else {
                        throw new Error();
                    }
                })
                .catch(error => {
                    // console.log("Inside the error block");
                    // console.log(error);
                    reject(error);
                });
        });
    },

    confirmReturnBook: (updateInfo, database) => {
        let tempData;
        let removeBookIds = [];
        let removeUids = [];
        for (let index = 0; index < updateInfo.length; ++index) {
            removeBookIds.push(updateInfo[index]._id);
            removeUids.push(updateInfo[index].uid);
        }
        return new Promise((resolve, reject) => {
            // console.log("In the promise");
            database.collection('returnCart')
                .remove({
                    _id: {$in: removeBookIds}
                })
                .then(result => {
                    // console.log("element found");
                    // console.log(result.result.n);
                    // resolve(result);
                    if (result) {
                        if (result.result) {
                            if (result.result.n) {
                                if (result.result.n > 0) {
                                    // console.log("executing student update");
                                    for (let index2 = 0; index2 < updateInfo.length; ++index2) {
                                        // console.log("updating student : " + (index2 + 1));
                                        database.collection(updateInfo[index2].loginType)
                                            .update({
                                                _id: updateInfo[index2].uid,
                                                "issuedBooks.bookId": updateInfo[index2]._id,
                                                $or: [{returnDate: {$exists: false}}, {returnDate: null}]
                                            }, {
                                                $inc: {issuedBookNos: -1},
                                                $set: {
                                                    "issuedBooks.$.returnDate": Date.now(),
                                                    "issuedBooks.$.fine": updateInfo[index2].fine
                                                }
                                            }, {
                                                $upsert: false
                                            });
                                    }
                                    return true;
                                }
                            }
                        }
                    }
                })
                .then(result => {
                    // console.log(result);
                    if (result) {
                        // console.log("executing book update");
                        for (let index = 0; index < updateInfo.length; ++index) {
                            // console.log("updating book : " + (index + 1));
                            database.collection('book')
                                .update({
                                    _id: updateInfo[index]._id,
                                    "issuedIds.uid": updateInfo[index].uid,
                                    $or: [{returnDate: {$exists: false}}, {returnDate: null}]
                                }, {
                                    $set: {
                                        "issuedIds.$.returnDate": Date.now(),
                                        "issuedIds.$.fine": updateInfo[index].fine,
                                        issued: false
                                    }
                                }, {
                                    $upsert: false
                                });
                        }
                        return true;
                    }
                })
                .then(result => {
                    // console.log("Successfully added to book");
                    // console.log(result);
                    resolve(result);
                })
                .catch(error => {
                    // console.log("Inside the error block");
                    // console.log(error);
                    reject(error);
                });
        });
    },
};