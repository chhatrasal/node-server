const config = require('./config');
const MongoClient = require('mongodb').MongoClient;

let app = require('./app');

MongoClient.connect('mongodb://' + config.mongoDB.host + ':' +
    config.mongoDB.port)
    .then(function (database) {

        app.set('database', database.db('Library'));
        // console.log(database);

        app.listen(config.express.port, config.express.host, function (error) {
            if (error) {
                console.log(error);
            } else {
                console.log("server is up and running......");
            }
        });
        console.log('mongodb server is up')
    }).catch(console.log);